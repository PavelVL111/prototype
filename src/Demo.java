
public class Demo {
    public static void main(String[] args) {

        ShapeCache shapeCache = new ShapeCache();
        shapeCache.load();
        Shape shape1 = shapeCache.getShape(1);
        Shape shape2 = shapeCache.getShape(1);
        System.out.println(shape1.equals(shape2));

        Shape shape3 = shapeCache.getShape(2);
        System.out.println(shape1.equals(shape3));

        Shape shape4 = shapeCache.getShape(3);
        System.out.println(shape1.equals(shape4));

    }


}

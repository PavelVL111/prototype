import java.util.HashMap;
import java.util.Map;

public class ShapeCache {
    private Map<Integer, Shape> store = new HashMap<>();

    public void load(){
        Shape shape1 = new Rectangle();
        shape1.setId(1);
        Shape shape2 = new Square();
        shape2.setId(2);
        Shape shape3 = new Circle();
        shape3.setId(3);
        store.put(shape1.getId(), shape1);
        store.put(shape2.getId(), shape2);
        store.put(shape3.getId(), shape3);
    }

    public Shape getShape(int id){
        return (Shape) store.get(id).clone();
    }
}
